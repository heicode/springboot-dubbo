package com.zy.zeyigou.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Dynamic;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbItem implements Serializable {
   @Id
    private Long id;
   @Field("item_title")
    private String title;

    private String sellPoint;
    @Field("item_price")
    private BigDecimal price;

    private Integer stockCount;

    private Integer num;

    private String barcode;
    @Field("item_image")
    private String image;

    private Long categoryid;

    private String status;

    private Date createTime;
    @Field("item_updateTime")
    private Date updateTime;

    private String itemSn;

    private BigDecimal costPirce;

    private BigDecimal marketPrice;

    private String isDefault;
    @Field("item_goodsid")
    private Long goodsId;

    private String sellerId;

    private String cartThumbnail;
    @Field("item_category")
    private String category;
    @Field("item_brand")
    private String brand;

    private String spec;
    @Field("item_seller")
    private String seller;
    //定义动态域
    @Dynamic
    @Field("item_spec_*")
    private Map<String,String> specMap;

}