package com.zy.zeyigou.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbTypeTemplate implements Serializable {
    @Id
    private Long id;

    private String name;

    private String specIds;

    private String brandIds;

    private String customAttributeItems;

}