package com.zy.zeyigou.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbSeckillOrder implements Serializable{
    @Id
    private Long id;

    private Long seckillId;

    private BigDecimal money;

    private String userId;

    private String sellerId;

    private Date createTime;

    private Date payTime;

    private String status;

    private String receiverAddress;

    private String receiverMobile;

    private String receiver;

    private String transactionId;

}