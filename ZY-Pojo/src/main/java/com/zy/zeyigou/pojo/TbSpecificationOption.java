package com.zy.zeyigou.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbSpecificationOption implements Serializable {
    @Id
    private Long id;

    private String optionName;

    private Long specId;

    private Integer orders;

}