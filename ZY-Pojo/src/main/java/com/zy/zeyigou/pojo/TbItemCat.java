package com.zy.zeyigou.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbItemCat implements Serializable {
    @Id
    private Long id;

    private Long parentId;

    private String name;

    private Long typeId;

}