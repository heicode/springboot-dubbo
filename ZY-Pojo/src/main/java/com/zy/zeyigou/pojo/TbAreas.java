package com.zy.zeyigou.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbAreas implements Serializable {
    @Id
    private Integer id;

    private String areaid;

    private String area;

    private String cityid;

}