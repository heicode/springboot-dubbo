package com.zy.zeyigou.group;



import com.zy.zeyigou.pojo.TbGoods;
import com.zy.zeyigou.pojo.TbGoodsDesc;
import com.zy.zeyigou.pojo.TbItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Feng.Wang
 * @Company: Zelin.ShenZhen
 * @Description: 组合类
 * @Date: Create in 2019/4/26 09:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goods implements Serializable {
    private TbGoods goods;
    private TbGoodsDesc goodsDesc;
    private List<TbItem> items;
}
