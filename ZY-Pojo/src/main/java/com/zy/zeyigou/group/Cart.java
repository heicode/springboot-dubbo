package com.zy.zeyigou.group;





import com.zy.zeyigou.pojo.TbOrderItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Feng.Wang
 * @Company: Zelin.ShenZhen
 * @Description:购物车类
 * @Date: Create in 2019/5/11 09:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart implements Serializable {
    private String sellerId;                    //商家id
    private String sellerName;                  //商家名称
    private List<TbOrderItem> orderItemList;    //订单项列表
}
