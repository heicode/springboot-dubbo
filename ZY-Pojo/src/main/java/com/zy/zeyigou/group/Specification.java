package com.zy.zeyigou.group;


import com.zy.zeyigou.pojo.TbSpecification;
import com.zy.zeyigou.pojo.TbSpecificationOption;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Feng.Wang
 * @Company: Zelin.ShenZhen
 * @Description:  用于组合规格及规格选项的类
 * @Date: Create in 2019/4/24 09:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Specification implements Serializable {
    private TbSpecification spec;
    private List<TbSpecificationOption> specificationOptionList;
}
