app.service("brandService",function($http){
    //1.查询所有品牌
    this.findAll=()=>{
        return $http.get("../brand/list");
    }
    //2.分页查询
    this.findByPage=(page,pageSize)=>{
        return $http.get("../brand/findByPage?page="+page+"&pageSize="+pageSize);

    }
    //3.分页带条件查询
    this.search=(page,pageSize,entity)=>{
        return $http.post("../brand/search?page="+page+"&pageSize="+pageSize,entity);
    }
    //4.保存品牌
    this.save=(url,entity)=>{
        return $http.post(url,entity);
    }
    //5.删除品牌
    this.del=(ids)=>{
        return $http.get("../brand/delete?ids="+ids);
    }
})