package com.zy.manager.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.zy.service.BrandService;
import com.zy.zeyigou.pojo.PageResult;
import com.zy.zeyigou.pojo.Result;
import com.zy.zeyigou.pojo.TbBrand;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("brand")
public class BrandController {

@Reference
    private BrandService brandService;

@RequestMapping("list")
    public List<TbBrand> list(){
    System.out.println(" list ");

    return  brandService.findAll();
}
    //2.分页查询
    @RequestMapping("findByPage")
    public PageResult<TbBrand> findByPage(@RequestParam(required = true,defaultValue = "1") int page, int pageSize){
        return brandService.findBypage(page,pageSize);
    }


    //3.分页带条件查询
    @RequestMapping("search")
    public PageResult<TbBrand> search(int page, int pageSize, @RequestBody(required = false) TbBrand brand) {
    return     brandService.search(page, pageSize, brand);

    }
    //4.添加
    @RequestMapping("add")
    public Result add(@RequestBody TbBrand brand){
        System.out.println("brand = " + brand);

        try {
            brandService.add(brand);
            return new Result(true,"添加成功！");
        } catch (Exception e) {
            e.printStackTrace();
        return new Result(false,"添加失败");
        }
    }

    //5.修改品牌
    @RequestMapping("update")
    public Result update(@RequestBody TbBrand brand){

        try {
            brandService.update(brand);
        return new Result(true,"修改成功！");

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"修改失败！");
        }
    }
    //6.删除品牌
    @RequestMapping("delete")
    public Result delete(String[] ids){
        try {
            brandService.delete(ids);
        return  new Result(true,"删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除失败！");
        }

    }
}
