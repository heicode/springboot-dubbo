package com.zy.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zy.mapper.TbBrandMapper;
import com.zy.service.BrandService;
import com.zy.zeyigou.pojo.PageResult;
import com.zy.zeyigou.pojo.Result;
import com.zy.zeyigou.pojo.TbBrand;
import com.zy.zeyigou.pojo.TbBrandExample;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private TbBrandMapper brandMapperr;

    //1.品牌列表
    public List<TbBrand> findAll() {
        return brandMapperr.selectAll();

    }
    //2.分页查询

    public PageResult<TbBrand> findBypage(int page, int pageSize) {
        //2.1)开始分页
        PageHelper.startPage(page, pageSize);
        //2.2)进行实例查询
        TbBrandExample example = new TbBrandExample();
        List<TbBrand> tbBrands = brandMapperr.selectByExample(example);
        //2.3)转换为Page<TbBrand>对象
        Page<TbBrand> brandPage = (Page<TbBrand>) tbBrands;
        //2.4)返回PageResult对象
        return new PageResult<TbBrand>(brandPage.getTotal(), brandPage.getResult());
    }
    //3.带条件的分页查询

    public PageResult<TbBrand> search(int page, int pageSize, TbBrand brand) {
        //3.1）开始分页
        PageHelper.startPage(page, pageSize);
        //3.2）获取实例
        TbBrandExample brandExample = new TbBrandExample();
        //3.3）添加条件
        TbBrandExample.Criteria criteria = brandExample.createCriteria();
        if (brand != null) {
            if (StringUtils.isNotBlank(brand.getName())) {
                criteria.andNameLike("%" + brand.getName() + "%");
            }
            if (StringUtils.isNotBlank(brand.getFirstChar())) {
                criteria.andFirstCharEqualTo(brand.getFirstChar());

            }

        }
        //3.4）进行实例查询
        List<TbBrand> brands = brandMapperr.selectByExample(brandExample);
        Page<TbBrand> brandPage = (Page<TbBrand>) brands;

        return new PageResult<TbBrand>(brandPage.getTotal(), brandPage.getResult());


    }

    //4.添加品牌
    public void add(TbBrand brand) {
        brandMapperr.insert(brand);
    }

    //5.修改品牌
    public void update(TbBrand brand) {
        brandMapperr.updateByPrimaryKey(brand);
    }
     //6.删除品牌
    public void delete(String[] ids) {
        if (ids != null) {
            for (String id : ids) {
                brandMapperr.deleteByPrimaryKey(id);
            }
        }

    }
}
