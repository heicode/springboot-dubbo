package com.zy.sellergoods.service;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.zy.mapper")
public class SellergoodsServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SellergoodsServiceApplication.class);
    }
}
