package com.zy.service;

import com.zy.zeyigou.pojo.PageResult;
import com.zy.zeyigou.pojo.TbBrand;

import java.util.List;

public interface BrandService {
    List<TbBrand> findAll();

    PageResult<TbBrand> findBypage(int page, int pageSize);

    PageResult<TbBrand> search(int page, int pageSize, TbBrand brand);

    void add(TbBrand brand);

    void update(TbBrand brand);

    void delete(String[] ids);
}
