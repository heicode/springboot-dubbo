package com.zy.mapper;


import com.zy.zeyigou.pojo.TbContentCategory;
import tk.mybatis.mapper.common.Mapper;

public interface TbContentCategoryMapper extends Mapper<TbContentCategory> {

}