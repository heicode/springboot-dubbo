package com.zy.mapper;


import com.zy.zeyigou.pojo.TbSpecificationOption;
import tk.mybatis.mapper.common.Mapper;

public interface TbSpecificationOptionMapper extends Mapper<TbSpecificationOption> {

}