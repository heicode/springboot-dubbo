package com.zy.mapper;




import com.zy.zeyigou.pojo.TbBrand;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TbBrandMapper extends Mapper<TbBrand> {

    //查询所有的品牌列表
    List<Map> selectBrandList();
}