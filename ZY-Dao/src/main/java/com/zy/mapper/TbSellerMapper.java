package com.zy.mapper;


import com.zy.zeyigou.pojo.TbSeller;
import tk.mybatis.mapper.common.Mapper;

public interface TbSellerMapper extends Mapper<TbSeller> {

}